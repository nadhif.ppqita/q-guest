const { sql } = require('@vercel/postgres');
require('dotenv').config({ path: '.env.development.local' });

async function execute() {
    // Drop existing table if it exists
    const deleteTable = await sql`DROP TABLE IF EXISTS guest`;

    // Create extension if not exists
    await sql`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`;

    // Create table with UUID primary key
    const createTable = await sql`
        CREATE TABLE IF NOT EXISTS guest (
            id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            nama VARCHAR(100) NOT NULL,
            email VARCHAR(255) NOT NULL,
            telp VARCHAR(15) NOT NULL,
            antrian SERIAL,
            kepentingan VARCHAR(255) NOT NULL
        )
    `;
    console.log(createTable);
}


execute().catch(err => console.error('Error executing script:', err));
