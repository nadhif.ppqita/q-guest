const { sql } = require('@vercel/postgres');
require('dotenv').config({ path: '.env.development.local' });

async function insertData() {
    try {
        const addData =await sql`
            INSERT INTO guest (nama, email, telp, kepentingan)
            VALUES ('John Doe', 'john@example.com', '123456789', 'Important')
        `;
        console.log(addData);
    } catch (error) {
        console.error('Error inserting data:', error);
    }
}

insertData();
