import { useRouter } from 'next/router';
import { useState } from 'react';

export default function AddData() {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [telp, setTelp] = useState('');
  const [kepentingan, setKepentingan] = useState('');
  const router = useRouter();
  
  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!nama || !email || !telp || !kepentingan) {
      alert('Semua kolom harus diisi');
      return;
    }

    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!regexEmail.test(email)) {
      alert('Format email tidak valid');
      return;
    }

    const regexTelp = /^\+62\d{9,12}$/;
    if (!regexTelp.test(telp)) {
      alert('Format telepon tidak valid (+62xxxxxxxxxxx)');
      return;
    }

    try {
      const response = await fetch('/api/insertData', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          nama: nama,
          email: email,
          telp: telp,
          kepentingan: kepentingan,
        }),
      });

      if (!response.ok) {
        throw new Error('Gagal menambah data.');
      }

      const json = await response.json();
      alert('Data berhasil ditambah');
      router.push('/');
    } catch (error) {
      console.error('Error saat menambah data', error.message);
      alert('Error saat menambah data: ' + error.message);
    }
  };

  return (
    <div>
      <div style={{display:"flex", alignItems:"center", justifyContent:"space-between", margin:"40px 70px 40px 70px"}}>
      <div>
      <h1 className="text-3xl font-bold mt-6 text-center">Tambah Data</h1>
      </div>
      <div>
      <div>
            <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/`)
              }}
            >
              Q-Guest
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/404.js`)
              }}
            >
              404-Page
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px' }} 
              onClick={() => {
                router.push(`https://opinenet.vercel.app/`)
              }}
            >
              Rate Us
            </button>
          </div>
      </div>
      </div>
      <form onSubmit={handleSubmit} className="rounded-lg p-20 pt-10 space-y-4">
        <div>
          <input
            type="text"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            placeholder='Masukkan nama lengkap Anda'
            required
          />
        </div>
        <div>
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            placeholder='Masukkan email'
            required
          />
        </div>
        <div>
          <input
            type="text"
            value={telp}
            onChange={(e) => setTelp(e.target.value)}
            placeholder='Masukkan nomor WhatsApp'
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            required
          />
        </div>
        <div>
          <textarea
            value={kepentingan}
            onChange={(e) => setKepentingan(e.target.value)}
            className="mt-1 p-2 border border-gray-300 rounded w-full"
            placeholder='Masukkan kepentingan Anda'
            required
          />
        </div>
        <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded w-full">
          Tambah Data
        </button>
      </form>
    </div>
  );
}
