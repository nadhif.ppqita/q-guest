import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import Swal from 'sweetalert2';

export default function Home() {
  const [data, setData] = useState(null);
  const router = useRouter();
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(`/api/getData`)
      .then((res) => res.json())
      .then((data) => {
        if (data.data) {
          setData(data.data);
        } else {
          setError(data.message || "Gagal mendapatkan data.");
        }
      })
      .catch((err) => {
        setError("Hubungi saya nek error");
        console.log("Gada Data jadinya error", err.message);
      });
  }, []);

  const handleDetailClick = (idDetail) => {
    fetch(`/api/getDataDetail?id=${idDetail}`)
      .then((res) => res.json())
      .then((data) => {
        if (!data.data) {
          Swal.fire("Tidak ada data", "Data tidak ditemukan di database", "error");
          return;
        }
  
        // Prepare detail content
        const detailContent = `
          <div style="font-family: Arial, sans-serif; color: #333; line-height: 1.6; text-align: left;">
            <div style="display: flex; justify-content: space-between; margin-bottom: 10px;">
              <div style="font-weight: bold;">ID:</div>
              <div>${data.data.id}</div>
            </div>
            <div style="display: flex; justify-content: space-between; margin-bottom: 10px;">
              <div style="font-weight: bold;">Nama:</div>
              <div>${data.data.nama}</div>
            </div>
            <div style="display: flex; justify-content: space-between; margin-bottom: 10px;">
              <div style="font-weight: bold;">Email:</div>
              <div>${data.data.email}</div>
            </div>
            <div style="display: flex; justify-content: space-between; margin-bottom: 10px;">
              <div style="font-weight: bold;">Telp:</div>
              <div>${data.data.telp}</div>
            </div>
            <div style="display: flex; justify-content: space-between; margin-bottom: 10px;">
              <div style="font-weight: bold;">Antrian:</div>
              <div>${data.data.antrian}</div>
            </div>
            <div style="display: flex; justify-content: space-between;">
              <div style="font-weight: bold;">Kepentingan:</div>
              <div>${data.data.kepentingan}</div>
            </div>
          </div>
        `;
  
        // Show SweetAlert with detail content and print button
        Swal.fire({
          title: 'Detail Antrian',
          html: detailContent,
          showCancelButton: true,
          confirmButtonText: 'Kembali',
          showCloseButton: true,
          showConfirmButton: false, // Hide default confirm button
          footer: `<button id="printButton" class="btn btn-primary" style="background-color: #4CAF50; color: white; border: none; padding: 10px 20px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer;">Cetak</button>`,
          customClass: {
            popup: 'swal-popup',
            content: 'swal-content',
            confirmButton: 'swal-confirm-button',
            cancelButton: 'swal-cancel-button',
            closeButton: 'swal-close-button'
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.cancel) {
            // Handle action when "Kembali" button is clicked
            console.log("Kembali button clicked");
          }
        });
  
        // Add click event listener to print button
        document.getElementById('printButton').addEventListener('click', () => {
          // Create a hidden iframe
          const iframe = document.createElement('iframe');
          iframe.style.position = 'absolute';
          iframe.style.width = '0';
          iframe.style.height = '0';
          iframe.style.border = 'none';
  
          // Append iframe to document body
          document.body.appendChild(iframe);
  
          // Write content to iframe and print
          const doc = iframe.contentWindow.document;
          doc.open();
          doc.write(`
            <html>
              <head>
                <title>Detail Antrian</title>
                <style>
                  body { font-family: Arial, sans-serif; color: #333; line-height: 1.6; }
                  .swal-content {
                    text-align: left;
                  }
                  .swal-content > div {
                    display: flex;
                    justify-content: space-between;
                    margin-bottom: 10px;
                  }
                  .swal-content > div > div:first-child {
                    font-weight: bold;
                  }
                  strong { font-weight: bold; }
                </style>
              </head>
              <body>
                ${detailContent}
              </body>
            </html>
          `);
          doc.close();
  
          // Wait for iframe to load before printing
          iframe.onload = () => {
            iframe.contentWindow.print();
            document.body.removeChild(iframe); // Remove iframe after printing
          };
        });
      })
      .catch((err) => {
        Swal.fire("Error", "Data tidak ditemukan: " + err.message, "error");
      });
  };
  

  return (
    <div className="flex justify-center">
      <div className="w-full lg:w-4/5">
      <div style={{display:"flex", alignItems:"center", justifyContent:"space-between", margin:"40px 0px 20px 0px"}}>
        <div>
      <h1 className="text-3xl p-5">Q-Guest</h1>
        </div>
        <div>
          <div>
          <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/add-data`)
              }}
            >
              Add Data
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/404.js`)
              }}
            >
              404-Page
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px' }} 
              onClick={() => {
                router.push(`https://opinenet.vercel.app/`)
              }}
            >
              Rate Us
            </button>
          </div>
        </div>
        
      </div>
        {error && <p>{error}</p>}
        {data ? (
          <div>
            <table className="text-center border-collapse border border-gray-600 w-full">
              <thead>
                <tr>
                  <th className="border border-gray-400 px-4 py-2">Nama</th>
                  <th className="border border-gray-400 px-4 py-2">Email</th>
                  <th className="border border-gray-400 px-4 py-2">Telp</th>
                  <th className="border border-gray-400 px-4 py-2">Kepentingan</th>
                  <th className="border border-gray-400 px-4 py-2">Action</th>
                </tr>
              </thead>
              <tbody>
                {data.map((item) => (
                  <tr key={item.id}>
                    <td className="border border-gray-400 px-4 py-2">{item.nama}</td>
                    <td className="border border-gray-400 px-4 py-2">{item.email}</td>
                    <td className="border border-gray-400 px-4 py-2">{item.telp}</td>
                    <td className="border border-gray-400 px-4 py-2">{item.kepentingan}</td>
                    <td className="border border-gray-400 px-4 py-2">
                      <button
                        style={{ border: "1px solid black", padding: "5px 25px", backgroundColor: "#90EE90" }}
                        onClick={() => handleDetailClick(item.id)}
                      >
                        Detail Antrian
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div></div>
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </div>
    </div>
  );
}
