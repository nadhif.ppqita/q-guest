import { useRouter } from "next/router";
// pages/404.js
export default function Custom404() {
    const router = useRouter();
    return (
      <div>
        <div style={{display:"flex", justifyContent:"flex-end", margin:"40px"}}>
        <div>
          <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/add-data`)
              }}
            >
              Add Data
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px', marginRight:"10px" }} 
              onClick={() => {
                router.push(`/`)
              }}
            >
              Q-Guest
            </button>
            <button 
              style={{ border: "1px solid", padding: '10px 30px' }} 
              onClick={() => {
                router.push(`https://opinenet.vercel.app/`)
              }}
            >
              Rate Us
            </button>
          </div>
          </div>
          <div style={{display:"flex", justifyContent:"center", margin:"130px"}}>
          <p style={{fontSize:"100px"}}><span style={{fontSize:"12px", textAlign:"center"}}>~ Page not found </span>404<span style={{fontSize:"12px", textAlign:"center"}}> Page not found ~</span></p>
          </div>
      </div>
    );
  }
  