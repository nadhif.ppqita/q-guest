const { sql } = require("@vercel/postgres");

export default async function handler(req, res) {
    try {
        if (req.method !== "GET") {
            return res.status(405).json({ message: "Method tidak diperbolehkan" });
        }

        const { id } = req.query;

        // Pastikan id yang diterima adalah string atau integer, sesuai dengan tipe data dalam database
        const { rows } = await sql`
            SELECT id, nama, email, telp, antrian, kepentingan 
            FROM guest 
            WHERE id = ${id}
        `;

        if (rows.length === 0) {
            return res.status(404).json({ message: "Data tidak ditemukan", error: true });
        }

        res.status(200).json({ message: "Success", data: rows[0] });
    } catch (error) {
        console.error("Terjadi error:", error);
        res.status(500).json({ message: "Terjadi error" });
    }
}
