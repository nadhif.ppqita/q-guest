const { sql } = require("@vercel/postgres");

async function insertData(req, res) {
    try {
        if (req.method !== "POST") {
            return res.status(405).json({ message: "Method tidak diperbolehkan" });
        }

        const { nama, email, telp, kepentingan } = req.body;

        if (!nama) {
            return res.status(400).json({ message: "Nama harus diisi" });
        }
        if (!email) {
            return res.status(400).json({ message: "Email harus diisi" });
        }
        if (!telp) {
            return res.status(400).json({ message: "Telepon harus diisi" });
        }
        if (!kepentingan) {
            return res.status(400).json({ message: "Kepentingan harus diisi" });
        }

        const regexTelp = /^\+62\d{9,12}$/;
        if (!regexTelp.test(telp)) {
            return res.status(400).json({ message: "Format telepon tidak valid (+62xxxxxxxxxxx)" });
        }

        const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!regexEmail.test(email)) {
            return res.status(400).json({ message: "Format email tidak valid" });
        }

        const rows = await sql`
            INSERT INTO guest (nama, email, telp, kepentingan)
            VALUES (${nama}, ${email}, ${telp}, ${kepentingan})
            RETURNING *`;

        res.status(200).json({ message: "Data berhasil ditambahkan", data: rows[0] });
    } catch (error) {
        console.error("Terjadi error:", error);
        res.status(500).json({ message: "Terjadi error dalam memproses permintaan" });
    }
}

export default insertData;
